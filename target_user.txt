=====================================================================
                         APSH2Aターゲット依存部 (asp-1.3.1対応）
                                  Last Modified: '08/05/13 
=====================================================================

○概要

APSH2Aターゲット依存部は，プロセッサにSH2A（SH7211）を搭載した，（株）
アルファプロジェクト製のAP-SH2A-0Aをサポートしている．


○カーネルの使用リソース

カーネルは以下のリソースを使用する．

  ・ROM
     ROM化の場合にコードを配置する．
     使用量は使用するAPIの数に依存する．

  ・RAM
     JTAGデバッグ時はコードとデータを，ROM化の場合はデータを配置する．
  	 使用量はオブジェクト数に依存する．
  
  ・CMT0
     カーネル内部のティックの生成に用いる．
     
  ・SCIFチャネル1
     コンソールの出力に使用．


○他のターゲットへの移植

APSH2Aターゲット依存部で使用するタイマやシリアルは，SH7211の内蔵機能の
みを使用するため，SH7211や近い構成のSH2Aを用いた環境には容易にポーティ
ングが可能である．ポーティングに関しては，以下の初期化ルーチンにターゲ
ット毎の初期化を追加すればよい．

  ・target_initialize() : target_config.c
    ターゲット依存部の初期化（C言語）
  ・_hardware_init_hook : target_support.S
    低レベルのターゲット依存の初期化
    スタートアップモジュールの中で，メモリの初期化の前に呼び出される


○デバッグ環境

デバッグ環境としては，JTAGデバッガの使用を前提とする．動作確認したデバ
ッガは，（株）アルファプロジェクト製の Xross Finder である．なお，JTAG
デバッグ時は，RAM上にカーネルのコードを置くため，ダウンロード前にRAMの
初期化が必要となる．RAMの初期化に関しては，AP-SH2A-0Aに付属している 
Xross Finder のスクリプトファイル（XrossFinder_sh2a_0a.xfc）を用いた．


○コンパイラ

GCC 4.1 で動作確認を行った．動作確認した GCC 4.1 は，以下のサイトから
バイナリパッケージをダウンロードして用いた．

   http://www.superh-tkernel.org/jpn/download/tools/index.html


○ボードの設定

本カーネル動作時の AP-SH2A-0A のディップスイッチの設定は以下の通りであ
る．

   SW1-1    : ON
   SW1-2    : OFF
   SW1-(3,4): MODE2
   SW1-5    : FlashROM書込み禁止
   SW1-6    : デバッグモードは実行環境により切り替える

注意事項：
　デバッグ時に「FlashROM書込み許可かつデバッグモード」（SW1-5：OFFかつ
SW1-6：ON）にすると,FRQCRが固定値となり、本来の動作クロックで動作しな
いので注意。
（SH7211自体の制限事項）


○コンソール出力

コンソール出力には，SCIFのチャネル1を用いる．AP-SH2A-0A 上ではSCIFのチ
ャネル1は，CN5に接続されている．通信フォーマットは以下の通りである．

  ・38400bps, Data 8bit, Parity none, Stop 1bit, Flow control none

○カーネル終了時の処理

ext_ker が呼び出されカーネル終了時には，apsh2a.h にある apsh2a_exit() 
が実行される．ディフォルトでは，無限ループが書かれている．独自の処理処
理を追加したい場合は，apsh2a_exit() の内容を書き換えること．


○アドレスマッピング

(1) ROM化時
　0x0000,0000 - 0x0007,0000 内蔵FlashROM 512KB
　　　　　　　　　　　　　　　・vectorセクション
　　　　　　　　　　　　　　　・textセクション
　　　　　　　　　　　　　　　・vector_entryセクション
　0x0c00,0000 - 0x0cff,ffff SRAM 16MB
　　　　　　　　　　　　　　　・dataセクション
　　　　　　　　　　　　　　　・bssセクション
　0xfff8,0000 - 0xfff8,7fff 内蔵RAM 32KB
　　　　　　　　　　　　　　　・非タスクコンテスト用スタック

(2) デバッグ時
　0x0c00,0000 - 0x0cff,ffff SRAM 16MB
　　　　　　　　　　　　　　　・vectorセクション
　　　　　　　　　　　　　　　・textセクション
　　　　　　　　　　　　　　　・vector_entryセクション
　　　　　　　　　　　　　　　・dataセクション
　　　　　　　　　　　　　　　・bssセクション
　0xfff8,0000 - 0xfff8,7fff 内蔵RAM 32KB
　　　　　　　　　　　　　　　・非タスクコンテスト用スタック

○ROM化

本カーネルはROM化をサポートしている．カーネルをフラッシュメモリに書き
込んで，実行するには Makefile.target にある ROM_BOOT を true に定義す
ればよい．


○各種設定の変更

幾つかのパラメータは変更可能になっている．設定ファイル毎に設定可能項目
は次のようになっている．

●Makefile.target の設定項目

・ROM_BOOT
  ROM化する場合はtrueに定義する

・GCC_TARGET
  GCCの suffix を定義

・TEXT_START_ADDRESS/DATA_START_ADDRESS
  テキストセクション，データセクションの開始アドレス

・DEFAULT_STK_TOP
  ディフォルトの非タスクコンテキスト用のスタックの終了番地
  　・非タスクコンテキスト用スタックポインタの初期値（底）

・INCLUDES
  インクルード指定

・COPTS
  Cコンパイラへのオプション

・LDFLAGS
  リンカへのオプション

●target_config.h の設定項目

・DEFAULT_ISTKSZ
  ディフォルトの非タスクコンテキスト用のスタックサイズ

・SIL_DLY_TIM1/2
  微少時間待ちのための定義


●target_syssvc.h の設定項目

・BPS_SETTING
  コンソールに使用するポート（SCIFのポート1）のボーレート

○ROM化の際にdataセクションのアドレスマッピングが不正になる問題
　・dataセクションのサイズが0の場合、bssセクションが
　　textセクションの直後に配置されてしまう。
　　（処理系の不具合？）
　・そこでダミー変数を用意して、dataセクションのサイズが0にならない
　　ようにして回避している。
　・ダミー変数がコンパイラの最適化で削除されるのを防ぐため、
　　アセンブラソースファイル（target_support.S）内にダミー変数_dummyを
　　記述している。


○変更履歴

'08/05/13 Release1.3.1
　・シリアルポートの割込み番号のマクロ名をsh3,sh4と合わせた
　　　SCIFx_RXI_INTNO, SCIFx_TXI_INTNO (x=0, 1, 2, 3)

'08/05/12 Release1.3.0
　・非依存部の変更内容に対応
　・最適化対策に無限ループにvolatile変数を用いていたのを元に戻した。
　　（while(i)をwhile(1)に変更）
　　　　・target_config.cのtarget_exit(void)
　　　　・apsh2a.hのapsh2a_exit(void)
　　　　・target_stddef.hのTOPPERS_assert_abort(void)

'08/03/13 Release1.1.2
　・HEW版と同期
　・target_support.S
　　　バスステートコントローラの設定の誤りを修正
　・target_config.h
　　　SIL_DLY_TIM1、SIL_DLY_TIM2の値の見直し
　・test_log.txtを新設　　asp/test/test_dlynse.cの実行結果を記録

'08/02/05 Release1.1.1
　・ROM化の際にdataセクションのアドレスマッピングが不正になる問題に対応
　・シリアルドライバ
　　　ポート番号毎に個別に割込み禁止／許可するように修正
　・target_config.c
　　・target_exit()
　　　　while(1)の無限ループが最適化により削除される可能性があるので、
　　　　volatile修飾した変数にアクセスするように修正


